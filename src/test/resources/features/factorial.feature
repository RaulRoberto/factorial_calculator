# language: en

@factorial
Feature: The Greatest Factorial Calculator
  As an user, I want to calculate correctly the factorial of integer numbers using the system.

  @app-name
  Scenario: Title of Application
    Given the user access the application
    Then the Tab Name is correctly shown
    And the Title is correctly shown

  @app-anchor-terms
  Scenario: Validating the anchor tags for Terms of Service
    Given the user access the application
    When Click in the Terms of Service link
    Then the user will redirect to Terms of Service page

  @app-anchor-privacy
  Scenario: Validating the anchor tags for Terms of Service
    Given the user access the application
    When Click in the Privacy link
    Then the user will redirect to Privacy page

  @calculate-factorial
  Scenario Outline: Validate Factorial Calculation
    Given the user access the application
    And enter with <value>
    When click to calculate
    Then the answer is <answer>

    Examples:
      | value                | answer                                           |
      | a                    | "Please enter an integer"                        |
      | @                    | "Please enter an integer"                        |
      | 0                    | "The factorial of 0 is: 1"                       |
      | 1                    | "The factorial of 1 is: 1"                       |
      | 150                  | "The factorial of 150 is: 5.713383956445855e+262"|
      | 171                  | "The factorial of 171 is: 1.24101807 E+309"      |
      | 200                  | "The factorial of 200 is: 7.886578673 E+374"     |
      | 991                  | "The factorial of 991 is: 4.17179837 E+2540      |
      | 992                  | "The factorial of 992 is: 4.138423983 E+2543"    |
      | -1                   | "The factorial of -1 is: -1"                     |

#      * 0-: The system returns error 500 - Internal Error, but doesn't show anything to the User, even the message labeled with the answer.
#      * 0 to 170: The system calculate correctly and shows the correct answer to the User.
#      * 171 to 991: the system returns the answer as "Infinity".
#      * 992+: The system returns error 500 - Internal Error, but doesn't show anything to the User, even the message labeled with the answer.

