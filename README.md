# Factorial Tests

Base project adapted to test factorial calculator for KIS test.
Project created using:
- Gherkin
- Cucumber
- Maven
- JDK 8
- Selenium Java 3.141.59
- Docker (rauldias98/maven-webdrivers)
  - Installed tools:
        Apache Maven 3.6.3
        JDK 11 and JDK 8
        11 is the default
  - Browsers:
        Chrome 114.0.5735.198
  - WebDrivers:
        ChromeDriver 114.0.5735.90
        _'/usr/bin/chromedriver'_

## To Run

```bash
  mvn test -Dtest=RunCucumber -Dbrowser=chrome 
```
## To Generate Report

```bash
  mvn cluecumber-report:reporting
```
    
